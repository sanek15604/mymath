package mymath

import "math"

func Sqrt(x float64) float64 {
	return math.Sqrt(x)
}

func Min(x, y float64) float64 {
	return math.Min(x, y)
}

func Max(x, y float64) float64 {
	return math.Max(x, y)
}

func Pow(x, y float64) float64 {
	return math.Pow(x, y)
}

func Floor(x float64) float64 {
	return math.Floor(x)
}

func Ceil(x float64) float64 {
	return math.Ceil(x)
}
